/**
 * Ngx-bootstrap, ng-materialize should be imported here.
 * All shared imports should be here
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import * as fromComponents from './components';
import * as fromDirectives from './directives';
import * as fromPipes from './pipes';

@NgModule({
    declarations: [
        ...fromComponents.components,
        ...fromDirectives.directives,
        ...fromPipes.pipes
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ...fromComponents.components,
        ...fromDirectives.directives,
        ...fromPipes.pipes
    ],
    providers: [
        ...fromPipes.pipes
    ],
})

export class SharedModule {}
