import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from 'src/app/main.component';

import { AUTH_ENABLED } from './../auth/auth-enabled';

// Add guards here if needed.
const guards = [];

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: AUTH_ENABLED ? guards : [],
    // Uncomment after generating the component
    // children: [
    //   {
    //     path: '',
    //     redirectTo: '', // Add redirect to here (project or what component you want too see first),
    //     pathMatch: 'full'
    //   },
    //   {
    //     path: '', // The path to your component,
    //     loadChildren: async () => (await import('the path to that directory')).ModuleName
    //   }
    // ],
    resolve: {
      // resolverName: ResolverName (don't forget to import)
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
